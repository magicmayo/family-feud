// import { questions, newGame } from "./game";

export default (io, ns) => {
    io.of(ns ? '/family-feud' : '/').on('connection', socket => {
        socket.on('teams', teams => {
            io.emit('teamNames', teams);
        });

        socket.on('submitAnswers', answers => {
            // questions(io, answers);
            io.emit('answers', answers);
        });

        socket.on('playing', side => {
            io.emit('play', side);
        });

        socket.on('showAllAnswers', (teamPlaying, stole) => {
            io.emit('flipEm', teamPlaying, stole);
        });

        socket.on('flipped', () => {
            io.emit('newRound');
        });

        socket.on('showAnswer', answer => {
            io.emit('flip', answer);
        });

        socket.on('steal', team => {
            const steals = team === 'left' ? 'right' : 'left';
            io.emit('stolen', steals);
        });

        socket.on('wrong', () => {
            io.emit('wrongAnswer')
        })
        socket.on('newGame', () => {
            io.emit('freshBoard')
        })
    });
}
