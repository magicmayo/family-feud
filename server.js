import express from "express";
import {createServer} from "http";
import {Server} from "socket.io"; 
import events from './sockets.js';
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();
const http = createServer(app);
const io = new Server(http, {
    cors: {
        origin: "http://localhost:3002",
        methods: ["GET", "POST"]
    }
});

const PORT = process.env.PORT || 3002;

app.use(express.json());

app.use(express.static("public"));

app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'public/index.html')));
app.get('/host', (req, res) => res.sendFile(path.join(__dirname, 'public/host.html')));

events(io);

http.listen(PORT, () => console.log(`Listening on PORT ${PORT}`));
