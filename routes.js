const path = '/home/pi/projects';
import express from 'express';

export const routes = app => {
    app.use(express.static(`${path}/family-feud/public`))
    app.get('/', (req, res) => res.sendFile(`${path}/family-feud/public/index.html`));
    app.get('/host', (req, res) => res.sendFile(`${path}/family-feud/public/host.html`));
}