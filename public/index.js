const buzzer = new Audio('./buzzer.wav');
const ding = new Audio('./ding.mp3');

document.body.onload = () => {
    const answerList = document.querySelector('.answers');
    const leftTeam = document.querySelector('.left_team');
    const rightTeam = document.querySelector('.right_team');
    const leftScore = document.querySelector('#left_score');
    const rightScore = document.querySelector('#right_score');
    const roundScore = document.querySelector('.score');
    const allAnswers = {};
    let teamPlaying = '';
    let incorrectAnswers = 0;
    let roundMultiplier = 1;
    let round = 1;

    const add = (numOne, numTwo) => {
        return parseInt(numOne) + parseInt(numTwo);
    }

    const flipAll = async () => {
        const revList = Object.keys(allAnswers).reverse();
        for(let i=0; i<revList.length; i++){
            const answer = allAnswers[revList[i]];
            if(!answer.flipped){
                await new Promise(p => {
                    setTimeout(() => {
                        showAnswer(answer);
                        p();
                    }, 3000)
                })
            }
        }
    }

    socket.on('teamNames', teams => {
        leftTeam.querySelector('.name').innerText = teams.left;
        rightTeam.querySelector('.name').innerText = teams.right;
    });

    socket.on('wrongAnswer', () => {
        if(incorrectAnswers < 3){
            incorrectAnswers++
            const X = document.createElement('span');
            X.innerText = 'X';
            if(teamPlaying === 'left') leftTeam.querySelector('.strike').append(X);
            else if(teamPlaying === 'right') rightTeam.querySelector('.strike').append(X);
            buzzer.play()
        } else {
            incorrectAnswers = 0;
        }
    });

    socket.on('play', team => {
        const left = team === 'left'; 
        if(left){
            teamPlaying = 'left';
            leftTeam.classList.add('playing');
            rightTeam.classList.remove('playing');
        } else {
            teamPlaying = 'right';
            leftTeam.classList.remove('playing');
            rightTeam.classList.add('playing');
        }
    })

    socket.on('answers', answers => {
        answerList.innerHTML = '';
        answers.forEach((answer, i) => {
            allAnswers[i+1] = answer;

            const answerCard = document.createElement('div');
            const answerInner = document.createElement('div');
            const answerFront = document.createElement('div');
            const answerBack = document.createElement('div');
            const answerNumber = document.createElement('div');
            const answerText = document.createElement('span');
            const answerPoints = document.createElement('span');

            answerInner.classList.add('answer_inner');
            answerFront.classList.add('answer_front');
            answerBack.classList.add('answer_back');
            answerText.classList.add('answer_text');
            answerPoints.classList.add('answer_points');

            answerText.innerText = answer.answer;
            answer.points = parseInt(answer.points) * roundMultiplier;
            answerPoints.innerText = answer.points;

            answerBack.append(answerText, answerPoints);
            answerFront.append(answerNumber);

            answerNumber.classList.add('answer_number');
            answerNumber.innerText = i+1;

            answerCard.dataset.answer = answer.answer;
            answerCard.classList.add('answer_card');

            answerInner.append(answerFront, answerBack);
            answerCard.append(answerInner);
            answerList.append(answerCard);
        });
    });

    const nextRound = (team, stole) => {
        if(team === 'left' && !stole){
            leftScore.innerText = add(leftScore.innerText, roundScore.innerText);
        } else if(!stole){
            rightScore.innerText = add(rightScore.innerText, roundScore.innerText);
        }

        round++;
        if(round > 2 && round < 5) roundMultiplier +=1;

        leftTeam.querySelector('.strike').innerHTML = '';
        rightTeam.querySelector('.strike').innerHTML = '';
        leftTeam.classList.remove('playing');
        rightTeam.classList.remove('playing');
        roundScore.innerText = 0;
        socket.emit('flipped');
    }
    
    socket.on('flipEm', (team, stole) => {
        flipAll();
        nextRound(team, stole);
    });
    
    socket.on('stolen', team => {
        if(team === 'left') leftScore.innerText = add(leftScore.innerText, roundScore.innerText);
        else rightScore.innerText = add(rightScore.innerText, roundScore.innerText);
        flipAll();
        nextRound(team, true)
        roundScore.innerText = 0;
    });

    socket.on('flip', (answer, team) => {
        showAnswer(answer, true, team);
        ding.play();
    })

    const showAnswer = (answer, guessing = false) => {
        const answerCard = document.querySelector(`[data-answer=${answer.answer}]`);
        answerCard.classList.add('flipped');
        allAnswers[answer.rank].flipped = true;
        
        if(guessing){
            roundScore.innerText = add(roundScore.innerText, (parseInt(answer.points) * roundMultiplier));
        }
        
        const answers = Object.keys(allAnswers);
        for(let i=0; i<answers.length; i++){
            if(!allAnswers[answers[i]].flipped) break;
            if(i === answers.length - 1){
                nextRound(teamPlaying);
            }
        }
    }
    
    const freshBoard = () => {
        leftTeam.querySelector('.name').innerHTML = '';
        rightTeam.querySelector('.name').innerHTML = '';
        leftTeam.querySelector('.strike').innerHTML = '';
        rightTeam.querySelector('.strike').innerHTML = '';
        answerList.innerHTML = '';
        teamPlaying = '';
        incorrectAnswers = 0;
        roundMultiplier = 1;
        round = 1;    
    }
    
    socket.on('freshBoard', freshBoard);
    socket.on('showAnswer', showAnswer);
};
