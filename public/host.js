document.body.onload = () => {
    const addAnswer = document.querySelector('.add_answer');
    const answersDiv = document.querySelector('.answers');
    const finishButton = document.querySelector('.finish');
    const showAnswers = document.querySelector('.show_answer_buttons');
    const showAllAnswers = document.querySelector('.show_all_answers');
    const container = document.querySelector('.container ');
    const answerList = document.querySelector('.answer_list');
    const submitNames = document.querySelector('.submit_names');
    const leftTeam = document.querySelector('.left_team');
    const rightTeam = document.querySelector('.right_team');
    const teamsDiv = document.querySelector('.teams');
    const steal = document.querySelector('.steal');
    const wrongAnswer = document.querySelector('.wrong_answer');
    const removeAnswer = document.querySelector('.remove');

    removeAnswer.addEventListener('click', () => {
        answerList.querySelector('.answer').remove();
    });

    const answers = [];
    let teamPlaying = '';
    let stole = false;

    const newRound = () => {
        teamPlaying = '';
        answerList.innerHTML = `Next Round!
            <div class="answer">
                <input value=''/>
                <input type="number" value=2 min=2 max=100 />
            </div>`
        showAnswers.innerHTML = '';
        answers.length = 0;
        container.classList.remove('hide');
        answersDiv.classList.add('hide');
    }

    const onlyAlpha = e => {
        console.log(e.key);
        if(e.key.match(/[^a-zA-Z ]/)){
            e.preventDefault();
        }
    }

    const play = side => {
        socket.emit('playing', side);
        teamPlaying = side;
    }

    steal.addEventListener('click', () => {
        stole = true;
        socket.emit('steal', teamPlaying, stole);
        newRound();

    })

    submitNames.addEventListener('click', () => {
        if(leftTeam.value === '' || rightTeam.value === ''){
            return confirm('You have to enter both team names!');
        }

        socket.emit('teams', {left: leftTeam.value, right: rightTeam.value});

        teamsDiv.classList.add('hide');

        const playDiv = document.createElement('div');
        const right = document.createElement('button');
        const left = document.createElement('button');

        left.innerText = leftTeam.value;
        right.innerText = rightTeam.value;
        left.addEventListener('click', () => play('left'));
        right.addEventListener('click', () => play('right'));

        playDiv.append(left, right);

        answersDiv.append(playDiv);
        container.classList.remove('hide');
    });

    answerList.children[0].children[0].addEventListener('onkeydown', onlyAlpha);
    addAnswer.addEventListener('click', () => {
        if(answerList.children.length < 8){
            const answer = document.createElement('input');
            const points = document.createElement('input');
            const div = document.createElement('div');
            const removeButton = document.createElement('img');

            removeButton.classList.add('remove');

            answer.addEventListener('onkeydown', onlyAlpha);

            points.type = 'number';
            points.max = '100';
            points.min = '2';
            points.value = 2;
            div.append(answer, points, removeButton);

            div.classList.add('answer');
            removeButton.addEventListener('click', () => div.remove());
            answerList.append(div);
        }
    });
    
    finishButton.addEventListener('click', () => {
        const answerDiv = document.querySelectorAll('.answer');
        let totalPoints = 0;
        const validationArr = [];
        answerDiv.forEach(answer => {
            const answerArr = Array.from(answer.children);
            let answerText = answerArr[0].value.replace(/[^a-zA-Z]/g, '');
            const points = answerArr[1].value;
            totalPoints = totalPoints + parseInt(points);
            validationArr.push(answerText);
            
            answers.push({answer: answerText, points: points});
        });
        if(totalPoints > 100){
            answers.length = 0;
            return confirm('You can only have 100 points between all your answers!  Please adjust your answer\'s points!');
        }
        for(let i=0; i<answerDiv.length; i++){
            if(i === validationArr.indexOf(answerDiv[i].children[0].value.split(' ').join('+'))) continue;
            if(validationArr.includes(answerDiv[i].children[0].value.split(' ').join('+'))){
                answers.length = 0;
                return confirm('You can\'t have the same answer more than once!');
            }
        }
        container.classList.add('hide');

        answers.sort((a,b) => b.points - a.points);
        answers.forEach((answer, i) => {
            const button = document.createElement('button');

            button.innerText = `Flip ${answer.answer}`;
            answers[i].rank = i+1;
            button.addEventListener('click', e => {
                socket.emit('showAnswer', {...answer, rank: i+1});
                button.remove();
            });

            showAnswers.append(button);
        });

        answersDiv.classList.remove('hide');

        socket.emit('submitAnswers', answers);
    });

    showAllAnswers.addEventListener('click', () => {
        socket.emit('showAllAnswers', teamPlaying, stole);
    });

    wrongAnswer.addEventListener('click', () => {
        socket.emit('wrong');
    });
    
    socket.on('newRound', newRound);
};